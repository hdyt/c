with import <nixpkgs> {}; stdenv.mkDerivation {
  name = "helloAlfian";
  src = ./.;
  buildInputs = [ coreutils gcc ];
  buildPhase = ''
    gcc "$src/hello.c" -o ./helloalf
  '';
  installPhase = ''
    mkdir -p "$out/bin"
    cp ./helloalf "$out/bin/"
  '';
}
